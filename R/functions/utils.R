# Author: Flavian Tschurr
# Project: Frost Damage Index
# Date: 06.07.2022
# Purpose:FDI: utils function
################################################################################


DAS_adder <- function(df,sowing_dates,years){
  df$DAS <- NA
  require(lubridate)
  df$timestamp <- as_datetime(df$timestamp)
  years_out <- list()
  for(yr in years){
    sowing_date_yr <- as.Date(sowing_dates[as.character(yr),],format="%d-%m-%Y")
    one_year <- subset(df, timestamp >= sowing_date_yr & timestamp <= (sowing_date_yr+days(360)))
    date_vect <- seq.Date(from= sowing_date_yr, to=(sowing_date_yr+days(360)), by="days")
    for(i in 1:length(one_year$timestamp)){
      one_year$DAS[i] <-     which(date_vect == as.Date(one_year$timestamp[i]))
      
      

    }

    one_year$year <- yr
    years_out[[as.character(yr)]] <- one_year
  }
  
  out <- do.call("rbind", years_out)
  return(out)
}




DAS_adder_hour <- function(df,sowing_dates,years){
  
  df$DAS <- NA
  require(lubridate)
  df$timestamp <- as_datetime(df$timestamp)
  years_out <- list()
  for(yr in years){
    
    sowing_date_yr <- as.Date(sowing_dates[as.character(yr),],format="%d-%m-%Y")
    one_year <- subset(df, timestamp >= sowing_date_yr & timestamp <= (sowing_date_yr+days(360)))
    date_vect <- seq.Date(from= sowing_date_yr, to=(sowing_date_yr+days(360)), by="days")
    for(i in 1:length(one_year$timestamp)){
      one_year$DAS[i] <-     which(date_vect == as.Date(one_year$timestamp[i])) 
      
      
      
      
    }
    one_year$DAS <- one_year$DAS + hour(one_year$timestamp)/24

    years_out[[as.character(yr)]] <- one_year
  }
  
  out <- do.call("rbind", years_out)
  return(out)
}







combined_data_cleaning_function <- function(one_measurement_unit,
                                            df_env,
                                            variable,
                                            env_variable,
                                            data_cleaning= NA ){
  #'@param one_measurement_unit dataframe containing values of one measurement unit (e.g. plot, genotype etc...)
  #'@param df_env dataframe with environmental data (contains values and a "timestamp" column --> containing the timestamps)
  #'@param variable measurement variable of the one_measurement_unit data frame
  #'@param env_variable env variable (e.g. tas_2m)
  #'@param data_cleaining options how data should be cleaned
  require(dplyr)
  source("R/functions/utils.R")
  

  # write all values into a list
  measurement_list <- sorting_env_to_list(one_measurement_unit, variable, df_env, env_variable)
  
  # data cleaning options
  if("no_negative_values" %in% data_cleaning){
    measurement_list <- remove_negative_values(measurement_list)
  }
  if("select_negative_values" %in% data_cleaning){
    measurement_list <- select_negative_values(measurement_list)
  }
  
  
  return(measurement_list)
}


sorting_env_to_list <- function(one_measurement_unit, variable, df_env, env_variable){
  #'@param one_measurement_unit df with measurements (needs a timestamp column)
  #'@param variable measurement variable (e.g. delta_CC)
  #'@param df_env environmental covariate (needs a timestamp column)
  #'@param env_variable env variable (e.g. tas_2m)
  #'
  require(dplyr)
  measurements_list <- list()

  for(measure in 2:length(one_measurement_unit[[variable]])){
    start_date <- one_measurement_unit$timestamp[(measure-1)]
    end_date <-  one_measurement_unit$timestamp[measure]
    df_env_subs <-  df_env %>%
      filter(timestamp <  end_date)%>%
      filter(timestamp > start_date)
    measurements_list[[as.character(one_measurement_unit[[variable]][measure])]] <- df_env_subs[[env_variable]]

  }
  
  return(measurements_list)
  
}

remove_negative_values <- function(measurement_list){
  #'@param measurement_list list, names = measurements, values within the list are the environmental data
  #'@description deletes all list entries with negative measurement values
  
  to_remove <- NULL
  for(i in 1:length(measurement_list)){
    if(as.numeric(names(measurement_list)[i])< 0){
      # measurements_list <- measurements_list[-i]
      to_remove <- append(to_remove,i)
    }
  }
  
  if(length(to_remove)>0){
    measurement_list <- measurement_list[-to_remove]
  }
  
  return(measurement_list)
  
}


################################################################################

select_negative_values <- function(measurement_list){
  #'@param measurement_list list, names = measurements, values within the list are the environmental data
  #'@description deletes all list entries with negative measurement values
  to_keep<- NULL
  for(i in 1:length(measurement_list)){
    if(as.numeric(names(measurement_list)[i])< 0){
      # measurements_list <- measurements_list[-i]
      to_keep <- append(to_keep,i)
    }
  }
  
  if(length(to_keep)>0){
    measurement_list <- measurement_list[to_keep]
  }
  
  return(measurement_list)
  
}

# skill scores

calc_RMSE <- function(measured, modelled){
  return(sqrt(mean((measured - modelled)^2)))
}

calc_RMSE_rel <- function(measured, modelled){
  diff_perc <- (modelled-measured)/measured*100
  return( sqrt(mean((diff_perc)^2)))
}

calc_MAE <- function(measured, modelled){
  return(abs(mean(measured - modelled)))
}

calc_MAE_rel <- function(measured, modelled){
  return(abs(mean((measured-modelled)/measured*100)))
}



moving_average <- function(data,step_size,sides){
  f_ <- rep(1/step_size,step_size)
  out <- stats::filter(data,f_,sides=sides)
  return(out)
}
